package main.java.com.dar10;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class SuperTest {
	private static final Logger logger = LogManager.getLogger(SuperTest.class);

	public static void main(String... Dar10) {
		logger.trace("Trace Message!");
		logger.debug("Debug Message!");
		logger.info("Info Message!");
		logger.warn("Warn Message!");
		logger.error("Error Message!");
		logger.fatal("Fatal Message!");
	}
}
package main.java.com.dar10;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RemoverWhiteSpaces {

	public static void main(String[] args) {
		String uno = "     abc";
		String dos = "  a b c ";
		String tres = "abc    ";
		
		uno = removiendoWS(uno);
		dos = removiendoWS(dos);
		tres = removiendoWS(tres);
		
		System.out.println(uno + ".");
		System.out.println(dos + ".");
		System.out.println(tres + ".");
		System.out.println("-");
	}
	
	public static String removiendoWS(String cadena){
		int i = cadena.length();
		while(i != 0){
			Pattern pattern = Pattern.compile("\\s$");
			Matcher matcher = pattern.matcher(cadena);
			while (matcher.find()) {
				cadena = cadena.substring(0, cadena.length()-1);
			}
			i--;
		}
		return cadena;
	}
}
package main.java.com.dar10;

public class Removedor {

    public static void main(String[] args) {
        String s = "abcdefghijklmnñoperstuvwxyz";
        char[] c = {'a', 'b', 'c'};

        String resultado = Removedor.removedorCharacters(s, c);
        System.out.println(resultado);
    }

    public static String removedorCharacters(String s, char[] c) {
        Integer longitudCadena = s.length();
        Integer longitudCaracter = c.length;
        StringBuffer buffer = new StringBuffer(longitudCadena);
        buffer.setLength(longitudCadena);
        Integer posicionActual = 0;
        for (Integer i = 0; i < longitudCadena; i++) {
            Character caracterActual = s.charAt(i);
            Boolean agregar = true;
            for(int j = 0; j < longitudCaracter; j++){
                if (caracterActual == c[j]){
                    agregar = false;
                    break;
                }
            }
            if(agregar){
                buffer.setCharAt(posicionActual++, caracterActual);
                agregar = false;
            }
        }
        String retorno = buffer.toString().substring(0, longitudCadena - longitudCaracter) ; 
        return retorno;
    }
}
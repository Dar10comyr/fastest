package main.java.com.dar10.prueba;

public class Multipunto {
	public static void main(String...LegenDar10){
		String multipunto = null;
		String idSucursal = "0, 706";

		metodoA("1", multipunto, idSucursal);
		multipunto = "0";
		metodoA("2", multipunto, idSucursal);
		multipunto = "1";
		metodoA("3", multipunto, idSucursal);
		idSucursal = "706";
		metodoA("4", multipunto, idSucursal);
		multipunto = "0";
		metodoA("5", multipunto, idSucursal);
		multipunto = null;
		metodoA("6", multipunto, idSucursal);
	}

	public static void metodoA(String numero, String multipunto, String idSucursal){
		if((multipunto == null || multipunto.contains("1")) 
				&& idSucursal.contains("0,")){
			System.out.println(numero + " paso por aqui!");
		}else
			System.out.println(numero);
		System.out.println("multipunto: " + multipunto);
		System.out.println("idSucursal: " + idSucursal);
	}
}

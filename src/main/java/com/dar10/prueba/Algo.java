package main.java.com.dar10.prueba;

import java.util.Calendar;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class Algo {
	private static final Logger logger = LogManager.getLogger(Algo.class);
	public static void main(String...dar10){
		logger.trace("Trace Message!");
		logger.debug("Debug Message!");
		logger.info("Info Message!");
		logger.warn("Warn Message!");
		logger.error("Error Message!");
		logger.fatal("Fatal Message!");
		
		Calendar cal = Calendar.getInstance();
		Long milis = cal.getTimeInMillis();
		
		System.out.println(milis + "");
		
	}
}
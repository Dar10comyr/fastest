package main.java.com.dar10;

public class ConsultasAjusteInventario {
	public static final String getEstado = "select Confirmado from Ajuste_Inventario where Numero_Comprobante = ?";

	public static final String getSetDatos = "SELECT a.Codigo_Articulo, a.Descripcion_articulo, "
			+ "Stock_Teorico, Stock_Real, Stock_Diferencia " + "FROM Ajuste_Inventario_Detalle aid, Articulos a "
			+ "WHERE Numero_Comprobante = ? and a.Codigo_Articulo = aid.codigo_Articulo";

	public static final String getNumeroCentroEmisor = "SELECT MAX(Numero_Comprobante) + 1 AS 'Próximo Número' FROM Ajuste_Inventario";

	public static final String getDetalleAjuste = "SELECT * FROM Ajuste_Inventario_Detalle WHERE Numero_Comprobante = ?";

	public static final String getDeposito = "SELECT Valor_Numerico_Parametro FROM parametros where Codigo_Parametro = 3";

	public static final String getSelectAjusteConcepto = "SELECT * FROM Conceptos_De_Ajustes;";

	public static final String getCodigoArticulo = "select Descripcion_Articulo, stock as Stock_Teorico from articulos where codigo_Articulo = ?";

	public static final String guardado04 = "select Numero_Comprobante from ajuste_Inventario where Numero_Comprobante = ?";

	public static final String guardado05_P1 = "Select top 1 Periodo, Turno from Cierres order by 1 desc";

	public static final String guardado05_P2_I = "insert into Transaccional values ( null,?,?,null,?,?,?,?,?,?,1)";

	public static final String guardado05_P2_II = "Select TOP 1 Codigo_Transaccional from Transaccional order by 1 desc";

	public static final String guardado05_P5_I = "select Valor_Numerico_Parametro from parametros where codigo_Parametro = 3";

	public static final String guardado05_P5_II = "select stock, precio, costo, costo_Ultima_Compra from articulos where codigo_Articulo = ? ";

	public static final String guardado05_P5_III = "update Articulos set stock = stock + ? where Codigo_Articulo = ?";

	public static final String guardado05_P5_IV = "select * from Articulos_Depositos where CodigoArticulo = ?";

	public static final String guardado05_P6_I = "Select codigo_Transaccional from Movimientos_Stock where codigo_Transaccional = ? "
			+ "and codigo_Articulo = ?";

	public static final String guardado05_P6_II = "insert into Movimientos_Stock values(?, ?, ?, ?, ?, ?, ?, ?)";

	public static final String guardado05_P6_III = "update Movimientos_Stock set Codigo_Transaccional = ? ,"
			+ " Codigo_Articulo = ?, Tipo_Transaccion = ?, Stock_Anterior = ?, "
			+ " Cantidad_Unidades = ?, Precio = ?, Costo = ?, Costo_Ultima_Compra = ? "
			+ " where codigo_transaccional = ?";

	public static final String guardado05_P6_IV = "select * from Movimientos_Stock_Depositos where CodigoArticulo = ?";
}
package main.java.com.dar10;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Calendar;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.pdf.PdfWriter;

public class EstiloPDF {

    public static void main(String[] args) {
        EstiloPDF objecto = new EstiloPDF();
        objecto.exportarPDF();

    }

public void exportarPDF() {
    Document documento = new Document();

    Calendar cal = Calendar.getInstance();
    Long milis = cal.getTimeInMillis();
    String nombreUnicoArchivo = "/Desktop/miPDF-" + milis + ".pdf";
    String rutaFinalArchivo = System.getProperty("user.home") + nombreUnicoArchivo;

    FileOutputStream fos;
    try {
        fos = new FileOutputStream(rutaFinalArchivo);

        PdfWriter pdfW = PdfWriter.getInstance(documento, fos);
        pdfW.setInitialLeading(20);

        documento.open();

        Paragraph paragraph = new Paragraph("dar10comyr\n.blogspot\n.com",
                FontFactory.getFont(FontFactory.COURIER, 72, Font.UNDERLINE, BaseColor.ORANGE));

        documento.add(paragraph);
    } catch (FileNotFoundException | DocumentException e) {
        e.printStackTrace();
    }
    documento.close();
}
}

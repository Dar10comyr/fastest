package main.java.com.dar10;

public class Exponer3Decimales {
	public static void main(String...LegenDar10){
		String numero = "1.0";
		String num2 = "123";
		String num3 = "0.123456789";

		numero = exponer3Decimales(numero);
		System.out.println(numero);

		num2 = exponer3Decimales(num2);
		System.out.println(num2);

		num3 = exponer3Decimales(num3);
		System.out.println(num3);
	}

	/** CONVIERTE UN STRING NUMERICO A UN DECIMAL CON 3 NUMEROS SI TIENE UN PUNTO*/
	public static String exponer3Decimales(String str){
		String aux ;
		if(str.contains(".")){
			aux = str.substring(str.indexOf("."));
			if(aux.length() < 3)
				aux = aux.concat("00");
			else if(aux.length() < 4)
				aux = aux.concat("0");
			else if(aux.length() > 4)
				aux = aux.substring(0, 4);

			str = str.substring(0, str.indexOf("."));
			str = str.concat(aux);
		}
		return str;
	}
}
package main.java.com.e;

public class DarioException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	private static final String mensajeDefault = "Papi ama a juli";

	public DarioException(String message) {
		super(message);
	}

	public DarioException() {
		super(mensajeDefault);
	}
}